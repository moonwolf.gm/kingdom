import 'package:kingdom/categories.dart';

class Category
{
  int _score;
  String _name;
  Categories _category;

  Category(String name, Categories category)
  {
    _name = name;
    _score = 0;
    _category = category;
  }

  modifyScore(int value)
  {
    _score += value;
  }

  int get score => _score;
}
