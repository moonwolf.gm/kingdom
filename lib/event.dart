import 'option.dart';

class Event
{
  String _desc;
  int _imageNo;
  List<Option> _options;

  Event(String desc, int image)
  {
    _desc = desc;
    _imageNo = image;
    _options = new List();
  }

  String get desc => _desc;
  int get icon => _imageNo;
  List<Option> get options => _options;

  addOption(Option option)
  {
    _options.add(option);
  }
}