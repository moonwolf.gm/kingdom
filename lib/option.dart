import 'package:kingdom/categories.dart';

class Option
{
  String _desc;
  Map<Categories, int> _scores;

  Option(String desc, Map<Categories, int> scores)
  {
    _desc = desc;
    _scores = scores;
  }

  addScore(Categories category, int score)
  {
    _scores[category] = score;
  }

  addScores(Map<Categories, int> scores)
  {
    _scores = scores;
  }

  String get desc => _desc;
  Map<Categories, int> get scores => _scores;

}