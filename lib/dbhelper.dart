import 'package:kingdom/categories.dart';
import 'package:kingdom/option.dart';

import 'event.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import "dart:io";
import 'package:flutter/services.dart';

class DBHelper
{
  Database _db;

  Future<Database> get db async
  {
    if (_db != null)
      return _db;
    _db = await _prepareDatabase();
    return _db;
  }

  _prepareDatabase() async
  {
    // Get a location using getDatabasesPath
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, "events.sqlite3");
    bool exists = await new File(path).exists();
    if (!exists) {
      var data = await rootBundle.load("assets/events.sqlite3");
      var list = data.buffer.asUint8List();
      //var out =
      await new File(path).writeAsBytes(list);
      //out.close();
    }
    Database db = await openDatabase(path, readOnly: true);
    return db;
  }

  Future<Map> getData() async
  {
    var dbClient = await db;
    List<Map> list = await dbClient.rawQuery('SELECT * FROM event WHERE id = 1');
    return list[0];
  }

  Future<Event> getEvent(int eventNo) async
  {
    var dbClient = await db;
    List<Map> list = await dbClient.rawQuery('SELECT * FROM event WHERE id = ?', [eventNo]);
    var ev = new Event(list[0]['description'], list[0]['icon']);
    var o = await getOptions(eventNo);
    o.forEach((option) => ev.addOption(option));
    return ev;
  }

  Future<List<Option>> getOptions(int eventID) async
  {
    var dbClient = await db;
    List<Map> list = await dbClient.rawQuery('SELECT * FROM option WHERE event_id = ?', [eventID]);
    List<Option> options = new List<Option>();
    for (var opt in list)
    {
      var scores = await getScores(opt['id']);
      options.add(Option(opt['description'], scores));
    }
    return options;
  }

  Future<Map<Categories, int>> getScores(int optionID) async
  {
    var dbClient = await db;
    List<Map> list = await dbClient.rawQuery('SELECT * FROM option_score WHERE option_id = ?', [optionID]);
    Map<Categories, int> scores = new Map();
    list.forEach((score) => scores[getScore(score['type_id'])] = score['score']);
    return scores;
  }

  Categories getScore(int scoreType)
  {
    Categories cat;
    switch (scoreType)
    {
      case 1:
        cat = Categories.CATEGORY_PEOPLE;
        break;
      case 2:
        cat = Categories.CATEGORY_RESOURCES;
        break;
      default:
        cat = Categories.CATEGORY_MILITARY;
        break;
    }
    return cat;
  }

  Future<int> count(String table) async
  {
    var dbClient = await db;
    return Sqflite
        .firstIntValue(await dbClient.rawQuery('SELECT COUNT(*) FROM ' + table));
  }
}
