import 'package:kingdom/categories.dart';
import 'package:kingdom/category.dart';
import 'package:kingdom/dbhelper.dart';
import 'package:kingdom/event.dart';
import 'package:kingdom/option.dart';

class Game
{
  int _currentEventNo;
  DBHelper _db;
  Map<Categories, Category> _categories;

  Game(DBHelper db)
  {
    _currentEventNo = 0;
    _db = db;
    _categories = new Map();
    _categories[Categories.CATEGORY_PEOPLE] = new Category("People", Categories.CATEGORY_PEOPLE);
    _categories[Categories.CATEGORY_RESOURCES] = new Category("Resources", Categories.CATEGORY_RESOURCES);
    _categories[Categories.CATEGORY_MILITARY] = new Category("Military", Categories.CATEGORY_MILITARY);
  }

  Future<Event> nextEvent()
  {
    _currentEventNo++;
    return getCurrentEvent();
  }

  Future<Event> getCurrentEvent() async
  {
    if (_currentEventNo > await _db.count('event') || _currentEventNo <= 0)
    {
      return null;
    }
    return _db.getEvent(_currentEventNo);
  }

  String answerEvent(Option option)
  {
    Map<Categories, int> score = option.scores;
    print(score);
    score.forEach((sc, score) => _categories[sc].modifyScore(score));
    return option.desc;
  }

  Map<Categories, Category> get categories => _categories;

  resetGame()
  {
    _currentEventNo = 0;
    _categories = new Map();
    _categories[Categories.CATEGORY_PEOPLE] = new Category("People", Categories.CATEGORY_PEOPLE);
    _categories[Categories.CATEGORY_RESOURCES] = new Category("Resources", Categories.CATEGORY_RESOURCES);
    _categories[Categories.CATEGORY_MILITARY] = new Category("Military", Categories.CATEGORY_MILITARY);
  }
}